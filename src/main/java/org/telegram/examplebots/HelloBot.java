package org.telegram.examplebots;

import org.telegram.abilitybots.api.bot.AbilityBot;
import org.telegram.abilitybots.api.objects.Ability;

import static org.telegram.abilitybots.api.objects.Locality.ALL;
import static org.telegram.abilitybots.api.objects.Locality.USER;
import static org.telegram.abilitybots.api.objects.Privacy.PUBLIC;

public class HelloBot extends AbilityBot {
  public static final String BOT_TOKEN = "694300157:AAFH20n0gsl2WWLb098cHzSte_Ugfyysss0";
  public static final String BOT_USERNAME = "birth_date_for_max_bot";

  public HelloBot() {
    super(BOT_TOKEN, BOT_USERNAME);
  }

  @Override
  public int creatorId() {
    return 123456789;
  }

  public Ability sayHelloWorld() {
    return Ability
        .builder()
        .name("hello")
        .info("says hello world!")
        .locality(USER)
        .privacy(PUBLIC)
        .action(ctx -> silent.send("Hello world!", ctx.chatId()))
        .build();
  }
}
